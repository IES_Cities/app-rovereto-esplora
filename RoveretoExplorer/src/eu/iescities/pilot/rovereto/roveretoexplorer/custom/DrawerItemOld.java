package eu.iescities.pilot.rovereto.roveretoexplorer.custom;

import android.graphics.drawable.Drawable;

public class DrawerItemOld {
	public String text;
	public Drawable icon;

	public DrawerItemOld(String text, Drawable icon) {
		super();
		this.text = text;
		this.icon = icon;
	}
}
