package eu.iescities.pilot.rovereto.roveretoexplorer.log;

public class LogConstants {
	
	public static final String APP_ID = "RoveretoExplorer";
	public static final String HOST = "http://150.241.239.65:8080";
	public static final String SERVICE_QUESTIONS_RESPONSE = "/IESCities/api/log/rating/response";
	public static final String SERVICE_LOG_RESPONSE = "/IESCities/api/log/app/event";
	public static final String LOG_PREFERENCES = "Log";
	public static final String SESSION_ID = "Session id";



}
