package eu.iescities.pilot.rovereto.roveretoexplorer.fragments.event.info;

public interface SaveEvents {
	public void saved(boolean returnvalue);
}
