package eu.iescities.pilot.rovereto.roveretoexplorer.fragments.questionnaire;

public interface QuizInterface {
	public void nextQuestion();
}
